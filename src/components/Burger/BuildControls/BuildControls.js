import React from 'react'
import classes from './BuildControls.module.css'
import BuildControl from './BuildControl/BuildControl'


const BuildControls = (props) => {
  // let controls = Object.keys(props.ingredients);
  let controls = [
    { label: 'Salad', type: 'salad' },
    { label: 'Bacon', type: 'bacon' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Meat', type: 'meat' }
  ]

  controls = controls.map((ctrl, i) => {
    // let ctrlUpLetter = ctrl.charAt(0).toUpperCase()+ctrl.slice(1);

    return <BuildControl
      key={ctrl.label}
      label={ctrl.label}
      added={() => props.addIngredient(ctrl.type)}
      removed={() => props.removeIngredient(ctrl.type)}
      disabledVal={props.disabledVal[i]}
    />
  })

  return (
    <div className={classes.BuildControls}>
      <p>Price: <strong>$ {props.totalPrice.toFixed(2)}</strong></p>
      {controls}
      <button
        className={classes.OrderButton}
        disabled={!props.purchasable}
        onClick={props.modalShow}>{props.isAuth ? 'Order Now' : 'Sign up to continue'}</button>
    </div>
  )
}

export default BuildControls;