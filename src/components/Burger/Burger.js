import React from 'react'
import classes from './Burger.module.css'
import BurgerIngredient from './BurgerIngredient/BurgerIngredient'

const burger = (props) => {
  let ingredients = props.ingredients;

  let finalIngredients = [];
  let transformedIngredients = Object.entries(ingredients);

  transformedIngredients.forEach(el => {
    let temp = el[1];
    while (temp > 0) {
      finalIngredients.push(el[0]);
      temp--;
    }
  })

  finalIngredients = finalIngredients.map((ing, i) => {
    return <BurgerIngredient key={ing + i} type={ing} />
  })

  if (finalIngredients.length === 0) {
    finalIngredients = <p>Please add your Ingredients</p>
  }

  return (
    <div className={classes.Burger}>
      <BurgerIngredient type="bread-top" />
      {finalIngredients}
      <BurgerIngredient type="bread-bottom" />
    </div>
  )
}

export default burger;