import React, { Component, Fragment } from 'react';
import Button from '../../UI/Button/Button';

class OrderSummary extends Component {
  render() {
    let ingredientsList = Object.keys(this.props.ingredientsList)
      .map(el => {
        return (
          <li key={el}>
            <strong>{el.charAt(0).toUpperCase() + el.slice(1)}:</strong> {this.props.ingredientsList[el]}
          </li>
        )
      })
    return (
      <Fragment>
        <h3>Your Order</h3>
        <p>A delicious burger with the following ingredients:</p>
        <ul>
          {ingredientsList}
        </ul>
        <hr />
        <p><strong>Total Price: $<em>{this.props.totalPrice.toFixed(2)}</em></strong></p>
        <p>Continue to checkout?</p>

        <Button buttonType='Success' clickButton={this.props.backdropShowHandler}>Cancel</Button>
        <Button buttonType='Danger' clickButton={this.props.continueShopHandler}>Continue</Button>
      </Fragment>
    )
  }
}

export default OrderSummary;