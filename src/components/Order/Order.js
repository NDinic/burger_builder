import React from 'react';
import classes from './Order.module.css';

const order = (props) => {
  const ingredients = [];
  for (let key in props.ingredients) {
    ingredients.push({
      ingName: key,
      ingAmount: props.ingredients[key]
    })
  }
  const ingredientOutput = ingredients.map(ing => (
    <span
      style={{
        textTransform: 'capitalize',
        display: 'inline-block',
        margin: '0 8px',
        border: '1px solid #ccc',
        padding: '5px'
      }}
      key={ing.ingName}>{ing.ingName} ({ing.ingAmount})</span>
  ));
  return (

    <div className={classes.Order} >
      <span onClick={() => props.clicked(props.id)} className={classes.CloseOrder}>X</span>
      <p>Ingredients: {ingredientOutput}</p>
      <p>Price: <strong>USD {Number.parseFloat(props.price).toFixed(2)}</strong></p>
    </div>
  )
}

export default order;