import React from 'react'
import classes from './Backdrop.module.css';

const backdrop = (props) => {
  return (
    props.backdropShow ? <div className={classes.Backdrop}
    onClick={props.backdropShowHandler}></div> : null
  )
}

export default backdrop;