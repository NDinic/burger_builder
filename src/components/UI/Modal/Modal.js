import React, { Component, Fragment } from 'react';
import classes from './Modal.module.css';
import Backdrop from '../Backdrop/Backdrop';

class Modal extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.modalShow !== this.props.modalShow || nextProps.children !== this.props.children) {
      return true;
    }
    return false;
  }

  render() {
    return (
      <Fragment>
        <Backdrop
          backdropShow={this.props.modalShow}
          backdropShowHandler={this.props.backdropShowHandler}
        />
        <div className={this.props.modalShow ? classes.Modal + ' ' + classes.showMe : classes.Modal}>
          {this.props.children}
        </div>
      </Fragment>
    )
  }
}

export default Modal;