import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import withErrorHandler from '../withErrorHandler/withErrorHandler';
import Spinner from '../../../src/components/UI/Spinner/Spinner';
import * as actions  from '../../store/actions/index';
import axios from '../../../src/axios-orders';

class BurgerBuilder extends Component {
  state = {
    modalShow: false
  }
  componentDidMount() {
    this.props.onInitIngredients()
  }
  updatePurchaseState(purchasedIngredients) {
    const sumPurchasedIngredients = Object.values(purchasedIngredients).reduce((acc, val) => acc + val, 0);
    return sumPurchasedIngredients > 0
  }

  modalShowHandler = () => {
    if(this.props.isAuthenticated) {
      this.setState({ modalShow: true })
    } else {
      this.props.onSetAuthRedirectPath('/checkout');
      this.props.history.push('/auth');
    }
    
  }

  backdropShowHandler = () => {
    this.setState({ modalShow: false })
  }
  continueShopHandler = () => {
    // alert('Nom Nom'); 
    this.props.onInitPurchase();
    this.props.history.push('/checkout')
  }
  render() {
    const disabledInfo = { ...this.props.ings };

    let disabledVal = [];
    Object.values(disabledInfo).forEach(val => {
      disabledVal.push(val <= 0)
    })

    // for ( let key in disabledInfo ) {
    //   disabledInfo[key] = disabledInfo[key] <= 0
    // }
    let orderSummary = null;

    let burger = this.props.error ? <p>Ingredients can't be loaded</p> : <Spinner />
    if (this.props.ings) {
      burger = (
        <Fragment>
          <Burger ingredients={this.props.ings} />
          <BuildControls
            addIngredient={this.props.onIngredientsAdded}
            removeIngredient={this.props.onIngredientsRemoved}
            disabledVal={disabledVal}
            ingredients={this.props.ings}
            totalPrice={this.props.price}
            purchasable={this.updatePurchaseState(this.props.ings)}
            modalShow={this.modalShowHandler}
            isAuth={this.props.isAuthenticated}
          />
        </Fragment>
      );
      orderSummary = <OrderSummary
        ingredientsList={this.props.ings}
        backdropShowHandler={this.backdropShowHandler}
        continueShopHandler={this.continueShopHandler}
        totalPrice={this.props.price}
      />
    }

    return (
      <Fragment>
        <Modal
          modalShow={this.state.modalShow}
          backdropShowHandler={this.backdropShowHandler}>
          {orderSummary}
        </Modal>
        {burger}
      </Fragment>
    )
  }
}
const mapStateToProps = state => {
  return {
    ings: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.totalPrice,
    error: state.burgerBuilder.error,
    isAuthenticated: state.auth.token !== null
  }
}
const mapDispatchToProps = dispatch => {
  return {
    onIngredientsAdded: (ingName) => dispatch(actions.addIngredient(ingName)),
    onIngredientsRemoved: (ingName) => dispatch(actions.removeIngredient(ingName)),
    onInitIngredients: () => dispatch(actions.initIngredients()),
    onInitPurchase: () => dispatch(actions.purchaseInit()),
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))

  }
}
export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));