import React, { Component } from 'react';
import { connect } from 'react-redux';
import Order from '../../components/Order/Order';
// import Axios from 'axios';
import axios from '../../axios-orders';
import withErrorHandler from '../withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions/index';
import Spinner from '../../components/UI/Spinner/Spinner';
import classes from './Orders.module.css';
class Orders extends Component {
  state = {
    orders: [],
    loader: false
  }
  componentDidMount() {
    this.props.onFetchOrders(this.props.token, this.props.userId);
  }
  deleteOrderId = (orderId) => {
    this.props.onDeleteOrder(orderId, this.props.token);
  }

  render() {
    let orders = <Spinner />
    if(!this.props.loading) {
      if(this.props.orders.length > 0){
        orders = this.props.orders.map(order => (
          <Order
            key={order.id}
            ingredients={order.ingredients}
            price={order.price}
            id={order.id}
            clicked={this.deleteOrderId} />
        ))
      }else{
        orders =  <h3 style={{ textAlign: 'center' }}>There are no orders !</h3>
      }
    }
    return (
      <div className={classes.OrdersWrapper}>
        {orders}
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    orders: state.order.orders,
    loading: state.order.loading,
    token: state.auth.token,
    userId: state.auth.userId
  }
}
const mapDispatchToProps = dispatch => {
  return {
    onFetchOrders: (token, userId) => dispatch(actions.fetchOrders(token, userId)),
    onDeleteOrder: (ordId, token) => dispatch(actions.deleteOrder(ordId, token))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Orders, axios));
