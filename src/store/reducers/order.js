import * as actionTypes from '../actions/actionsTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
  orders: [],
  loading: false,
  purchased: false
}

const purchaseInit = (state, action) => {
  return updateObject(state, { purchased: false })
}

const purchaseBurgerStart = (state, action) => {
  return updateObject(state, { loading: true })
}

const purchaseBurgerSuccess = (state, action) => {
  const newOrder = updateObject(action.orderData, { id: action.orderId });

  return updateObject(state, {
    loading: false,
    purchased: true,
    orders: state.orders.concat(newOrder)
  })
}

const purchasedBurgerFail = (state, action) => {
  return updateObject(state, { loading: false });
}

const fetchOrdersStart = (state, action) => {
  return updateObject(state, { loading: true });
}

const fetchOrderSuccess = (state, action) => {
  return updateObject(state, {
    orders: action.orders,
    loading: false
  })
}

const fetchOrderFail = (state, action) => {
  return updateObject(state, { loading: false });
}

const deleteOrder = (state, action) => {
  return updateObject(state, {
    loading: false,
    orders: state.orders.filter(order => order.id !== action.orderId)
  })
}
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.PURCHASE_INIT: return purchaseInit(state, action);
    case actionTypes.PURCHASE_BURGER_START: return purchaseBurgerStart(state, action);
    case actionTypes.PURCHASE_BURGER_SUCCESS: return purchaseBurgerSuccess(state, action);
    case actionTypes.PURCHASE_BURGER_FAIL: return purchasedBurgerFail(state, action);
    case actionTypes.FETCH_ORDERS_START: return fetchOrdersStart(state, action);
    case actionTypes.FETCH_ORDERS_SUCCESS: return fetchOrderSuccess(state, action);
    case actionTypes.FETCH_ORDERS_FAIL: return fetchOrderFail(state, action);
    case actionTypes.DELETE_ORDER: return deleteOrder(state, action);
    default: return state;
  }
}

export default reducer;